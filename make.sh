#!/usr/bin/env bash

# Shell script used as a "Makefile-like" helper tool for tests, generating coverage and docs.
# This script can be run using a standard installation of Git on Windows in the Git-BASH.

# ------------------------------------
# Python scripts
# ------------------------------------

BROWSER_PYSCRIPT="
import os, webbrowser, sys

webbrowser.open(os.path.abspath(sys.argv[1]))
"

OPEN_PYSCRIPT="
import os, subprocess, sys

filepath = sys.argv[1]
if sys.platform.startswith('darwin'): # macOS
    subprocess.call(('open', filepath))
elif os.name == 'posix': # Linux etc.
    subprocess.call(('xdg-open', filepath))
elif os.name == 'nt': # Windows
    filepath = os.path.join(*filepath.split('/'))
    os.startfile(filepath)
"

function docs_api() {
    # generate sphinx API docs
    rm -f docs/hvl_ccb*.rst
    sphinx-apidoc --separate -t docs/templates/apidoc -o docs/ src/hvl_ccb
}

# ------------------------------------
# for loop over all keywords given
# ------------------------------------

for KEYWORD in "$@"
do
    case "${KEYWORD}"
    in
        "help")
            # print help
            echo "This is a small helper script to trigger frequently used command sets. Usage:"
            echo "./make.sh keyword1 keyword2 ..."
            echo "Multiple keywords are executed in the sequence given."
            echo "The virtual environment has to be active."
            echo ""
            echo "style:"
            echo "    Run flake8 to check code style."
            echo ""
            echo "type:"
            echo "    Run mypy to do static type check using typing hints."
            echo ""
            echo "test:"
            echo "    Run py.test."
            echo ""
            echo "test-all:"
            echo "    Run tox."
            echo ""
            echo "coverage:"
            echo "    Run python tests and report code coverage in HTML format."
            echo ""
            echo "docs:"
            echo "    Generate sphinx HTML docs and open browser."
            echo ""
            echo "docs-pdf:"
            echo "    Generate sphinx PDF docs and open in browser. This needs a valid LaTeX installation."
            echo ""
            echo "format:"
            echo "    Formats all files in example, src/hvl_ccb and tests with 'black --preview'."
            echo ""
            echo "isort:"
            echo "    Sort all imports files in example, src/hvl_ccb and tests with 'isort .'"
            echo ""
            ;;

        "style")
            flake8 src tests
            ;;

        "type")
            mypy --show-error-codes src
            ;;

        "test")
            # run python tests in parallel
            py.test -n auto --dist=loadscope
            ;;

        "test-all")
            # run tox
            tox
            ;;

        "coverage")
            coverage run --source src/hvl_ccb -m pytest
            coverage report -m
            coverage html
            python -c "${BROWSER_PYSCRIPT}" htmlcov/index.html
            ;;

        "docs")
            # generate sphinx HTML docs and open browser
            docs_api
            rm -rf docs/_build
            python -msphinx -M html docs/ docs/_build
            python -c "${BROWSER_PYSCRIPT}" docs/_build/html/index.html
            ;;

        "docs-pdf")
            # generate sphinx PDF docs
            docs_api
            rm -rf docs/_build
            python -msphinx -M latexpdf docs/ docs/_build
            python -c "${OPEN_PYSCRIPT}" docs/_build/latex/hvl_ccb.pdf
            ;;

        "format")
            # Formats all files in example, hvl_ccb and tests with 'black --preview'
            black --preview src/ tests/ examples/
            ;;

        "isort")
            # Sort all imports files in example, hvl_ccb and tests with 'isort .'
            isort .
            ;;

        "ruff")
            # Check and format with ruff
            ruff check --fix src/ tests/ examples/ docs/
            ruff format src/ tests/ examples/ docs/
            ;;

        *)
            # default action if keyword not recognized
            echo "Keyword '${KEYWORD}' is not recognized."
            echo "Use './make.sh help' to get a list of possibilities"
    esac
done
