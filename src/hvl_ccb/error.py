#  Copyright (c) ETH Zurich, SIS ID and HVL D-ITET
#
"""
Introduce a common code base error for the CCB
"""


class CCBError(Exception):
    pass
