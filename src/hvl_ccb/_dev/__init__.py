#  Copyright (c) ETH Zurich, SIS ID and HVL D-ITET
#
"""Internal devices subpackage to avoid possible circular imports when sharing utilities
between `comm` and `dev` packages."""
