# Examples

Python scripts with example usages of `hvl_ccb` package. To install all 
additional requirements run:

    $ pip install -U pip
    $ pip install git+https://gitlab.com/ethz_hvl/hvl_ccb.git@TAG_OR_COMMIT#egg=hvl_ccb
    $ pip install -r examples/requirements.txt 

where `TAG_OR_COMMIT` might be any tag name from
[the list of project tags](https://gitlab.com/ethz_hvl/hvl_ccb/tags) or
[any commit hash](https://gitlab.com/ethz_hvl/hvl_ccb/commits/).
