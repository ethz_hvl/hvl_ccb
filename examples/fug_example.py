#  Copyright (c) ETH Zurich, SIS ID and HVL D-ITET
#
"""
Example script for the device FuG
"""

import logging
from time import sleep

from hvl_ccb.dev.fug import FuG

logging.basicConfig(level=logging.INFO)

# create device object
hv = FuG({"port": "COM3"})

# start the device
hv.start(max_voltage=1e3, max_current=10e-3)

# give the device some time to apply hv.set_number_of_recordings()
# that was called during hv.start()
sleep(0.5)

# print the nominal voltage and current
print(
    f"This power supply can apply {hv.max_voltage_hardware} V "
    f"and {hv.max_current_hardware} A"
)
print(f"In this setup the limits are at {hv.max_voltage} V and {hv.max_current} A")


# apply a current limitation
hv.current = 3.5e-3

sleep(0.5)

# turn on the output
hv.output = True
sleep(0.5)

# Try to apply a voltage which is too high
try:
    hv.voltage = 50000000
except ValueError:
    print(f"You can't set a voltage higher than {hv.max_voltage} V.")

# apply a (normal) voltage
SET_VOLTAGE = 250
hv.voltage = SET_VOLTAGE

sleep(0.5)  # give the device some time to apply the voltage

# check the set voltage
COMMAND_VOLTAGE = hv.set_voltage

print("Charging...")

# measure the output voltage
MAX_N = 25
while hv.current >= 1e-3 and MAX_N >= 0:
    print(f"still charging: {hv.voltage} V")
    MAX_N -= 1
    sleep(1)


# print the result
print(
    f"I wanted to set {SET_VOLTAGE} V, the command set {COMMAND_VOLTAGE} V, "
    f"and I finally measured {hv.voltage} V"
)

# stop the device (this also turns off the output)
hv.stop()
