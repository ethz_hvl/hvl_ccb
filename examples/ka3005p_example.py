#  Copyright (c) ETH Zurich, SIS ID and HVL D-ITET
#

from time import sleep

from hvl_ccb.dev.ka3000p import KA3000P


def main() -> None:
    ka3005p = KA3000P({"port": "COM5"})

    print("".center(88, "#"))
    print(" Connect to KA3005P ".center(88, "#"))
    print("".center(88, "#"))
    ka3005p.start()

    print("Connected to a power supply:")
    print(f"\t{'Brand':16}: {ka3005p.brand}")
    print(f"\t{'Serial Number':16}: {ka3005p.serial_number}")
    print(f"\t{'Max Voltage':16}: {ka3005p.max_voltage} V")
    print(f"\t{'Max Current':16}: {ka3005p.max_current} A")

    print("Lock manual usage via hardware buttons")
    ka3005p.lock = True

    try:
        ka3005p.voltage = 500
    except ValueError:
        print("Cannot set too high voltages")

    sleep(2)

    for ii in range(5):
        slot = ii + 1
        print(f"Save settings to slot {slot}")

        with ka3005p.save_settings_to(slot):
            ka3005p.voltage = slot * 2.5
            ka3005p.current = slot * 0.75

    sleep(2)

    for ii in range(5):
        slot = ii + 1
        print(f"Recall slot {slot}")
        ka3005p.recall = slot
        ka3005p.output = True
        print(f"\t{'Set Voltage':16}: {ka3005p.set_voltage} V")
        print(f"\t{'Set Current':16}: {ka3005p.set_current} A")
        print(f"\t{'Output':16}: {ka3005p.output}")
        print(f"\t{'Voltage':16}: {ka3005p.voltage} V")
        print(f"\t{'Current':16}: {ka3005p.current} A")

    ka3005p.output = False
    print("Unlock hardware buttons")
    ka3005p.lock = False

    print("".center(88, "#"))
    print(" Close connection to KA3005P ".center(88, "#"))
    print("".center(88, "#"))
    ka3005p.stop()


if __name__ == "__main__":
    main()
