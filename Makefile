.PHONY: $(MAKECMDGOALS)
.DEFAULT_GOAL := help

define BROWSER_PYSCRIPT
import os, webbrowser, sys, subprocess

try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

filepath = sys.argv[1]
if b"WSL" in subprocess.run(["uname", "-r"], capture_output=True).stdout:
	webbrowser.open(filepath)
else:
	webbrowser.open("file://" + pathname2url(os.path.abspath(filepath)))
endef
export BROWSER_PYSCRIPT

define OPEN_PYSCRIPT
import os, subprocess, sys

filepath = sys.argv[1]
if sys.platform.startswith('darwin'): # macOS
    subprocess.call(('open', filepath))
elif os.name == 'posix': # Linux etc.
    subprocess.call(('xdg-open', filepath))
elif os.name == 'nt': # Windows
    os.startfile(filepath)
endef
export OPEN_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

BROWSER := python -c "$$BROWSER_PYSCRIPT"
OPEN := python -c "$$OPEN_PYSCRIPT"

DOCKER := docker
DOCKER_IMAGE_TAG=hvl_ccb:devel_env
DOCKER_USER=devel
DOCKER_REPO_PATH=/home/$(DOCKER_USER)/hvl_ccb
REPO_PATH=$(realpath .)

define get_devel_env_id
	$(DOCKER) ps --filter "ancestor=$(DOCKER_IMAGE_TAG)" --format "{{.ID}}"
endef

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-pyc clean-test ## remove all build, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -rf {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

style: ## check style with flake8
	flake8 src tests

format: ## auto-format with black
	black --preview src tests example

isort: ## auto-format with isort
	isort .

ruff: ## check and format with ruff
	ruff check --fix src/ tests/ examples/ docs/
	ruff format src/ tests/ examples/ docs/

type: ## static code check using typing hints with mypy
	mypy --show-error-codes src

test: ## run tests quickly with the default Python
	py.test -n auto --dist=loadscope

test-all: ## run tests on every Python version with tox
	tox

coverage: ## check code coverage quickly with the default Python
	coverage run --source src -m pytest
	coverage report -m
	coverage html
	$(BROWSER) htmlcov/index.html

docs_api: ## generate Sphinx API docs
	rm -f docs/hvl_ccb*.rst
	sphinx-apidoc --separate -t docs/templates/apidoc -o docs/ src/hvl_ccb

docs: docs_api ## generate Sphinx HTML documentation, including API docs
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	$(BROWSER) docs/_build/html/index.html

docs_pdf: docs_api ## generate Sphinx PDF documentation, including API docs
	$(MAKE) -C docs clean
	$(MAKE) -C docs latexpdf
	$(OPEN) docs/_build/latex/hvl_ccb.pdf

servedocs: docs ## compile the docs watching for changes
	watchmedo shell-command -p '*.rst' -c '$(MAKE) -C docs html' -R -D .

release-check: dist ## package a release and check if release would be successful
	twine check --strict dist/*

release: dist ## package and upload a release
	twine upload dist/*

dist: clean ## builds source and wheel package
	python -m build --sdist --wheel
	ls -l dist

install: clean ## install the package to the active Python's site-packages
	pip install .[all]

build_devel_env: ## build devel env Docker image
	$(DOCKER) build --build-arg USER=$(DOCKER_USER) --build-arg REPO_PATH=$(DOCKER_REPO_PATH) -t $(DOCKER_IMAGE_TAG) .

run_devel_env: ## run a temp local devel env Docker container
	$(DOCKER) run --rm -i -t --mount type=bind,source="$(REPO_PATH)",target=$(DOCKER_REPO_PATH),consistency=cached $(DOCKER_IMAGE_TAG)

bash_devel_env: ## exec new Bash shell in a running local devel env Docker container
	$(DOCKER) exec -i -t $(shell $(call get_devel_env_id)) /bin/bash
