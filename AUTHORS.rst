=======
Credits
=======

Active Maintainers
------------------

* Chi-Ching Hsu <hsu@eeh.ee.ethz.ch>
* Henning Janssen <janssen@eeh.ee.ethz.ch>

Authors and Contributors
------------------------

* Pit Bechtold (Contributor)
* Fabian Bill (Author)
* Alise Chachereau (Author)
* Maria Del (Contributor, Maintainer)
* Joseph Engelbrecht (Author)
* Raphael Faerber (Contributor)
* David Graber (Author)
* Chi-Ching Hsu (Author, Maintainer)
* Henning Janssen (Author, Maintainer)
* Henrik Menne (Author, Maintainer)
* Luca Nembrini (Contributor)
* Mikołaj Rybiński (Author, Maintainer)
* Ruben Stadler (Contributor)
* David Taylor (Author)
* Hanut Vemulapalli (Contributor)
