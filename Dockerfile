# lowest supported
FROM python:3.10-bookworm

ARG USER=devel
ARG REPO_PATH=/home/$USER/hvl_ccb

# LabJack lib
RUN wget https://cdn.docsie.io/file/workspace_u4AEu22YJT50zKF8J/doc_VDWGWsJAhd453cYSI/boo_9BFzMKFachlhscG9Z/file_6ct0vbMEidvixvYCA/labjack_ljm_software_2019_07_16_x86_64tar.gz \
 && mv labjack_ljm_software_2019_07_16_x86_64tar.gz labjack_ljm_software_2019_07_16_x86_64.tar.gz \
 && tar -xzvf labjack_ljm_software_2019_07_16_x86_64.tar.gz \
# Docker workaround: use a flag to avoid restarting udev cf. https://labjack.com/comment/6419
 && bash labjack_ljm_software_2019_07_16_x86_64/labjack_ljm_installer.run -- --no-restart-device-rules \
 && rm -rf labjack_ljm_software_2019_07_16_x86_64/ labjack_ljm_software_2019_07_16_x86_64.tar.gz

# TiePie lib
RUN wget -q -O - http://packages.tiepie.com/public.key | apt-key add - \
 && bash -c 'echo "deb http://packages.tiepie.com/debian bullseye non-free" > /etc/apt/sources.list.d/packages.tiepie.com.list' \
 && apt-get update \
 && apt-get install -y libtiepie

## PicoTech SDK
RUN wget https://labs.picotech.com/debian/pool/main/libu/libusbpt104/libusbpt104_2.0.17-1r1441_amd64.deb \
  && apt-get update \
  && apt-get install -y apt-transport-https apt-utils udev ./libusbpt104_2.0.17-1r1441_amd64.deb \
  && ldconfig \
  && rm -rf libusbpt104_2.0.17-1r1441_amd64.deb

# docs & IDE
RUN apt-get update \
 && apt-get install -y graphviz vim

# Setup user
RUN useradd -m -s /bin/bash $USER
USER $USER

# Setup working directory and install dev requirements
RUN mkdir -p $REPO_PATH
COPY . $REPO_PATH
WORKDIR $REPO_PATH
ENV PATH="/home/$USER/.local/bin:${PATH}"
# See: https://github.com/picotech/picosdk-python-wrappers/pull/33
RUN pip install --user --no-cache-dir "PicoSDK@git+https://github.com/trybik/picosdk-python-wrappers.git@fix_lib_load_linux#egg=PicoSDK-1.0"
RUN pip install --user --no-cache-dir -U pip -e .[all] \
    && pip install --user --no-cache-dir -U pip -r requirements_dev.txt

CMD ["/bin/bash"]
