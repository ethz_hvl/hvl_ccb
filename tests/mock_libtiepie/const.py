#  Copyright (c) ETH Zurich, SIS ID and HVL D-ITET
#
"""
Mock constants
"""

MOCK_DEVICE_SERIAL_NUMBER = 34937  # Device with oscilloscope, generator and i2c
MOCK_I2CHOST_SERIAL_NUMBER = 35492  # Device with I2C host only
MOCK_GENERATOR_SERIAL_NUMBER = 23456  # Device with generator only
MOCK_OSCILLOSCOPE_SERIAL_NUMBER = 12345  # Device with oscilloscope only
MOCK_OSCILLOSCOPE_SERIAL_NUMBER_2 = 42345  # Oscilloscope without block measurement

MMB_BLOCK = 1
MM_BLOCK = 1 << MMB_BLOCK
