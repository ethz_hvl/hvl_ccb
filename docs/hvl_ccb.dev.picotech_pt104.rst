hvl\_ccb.dev.picotech\_pt104
============================

**NOTE**: `PicoSDK Python wrappers`_ already on import attempt to load the
`PicoSDK library`_; thus, the API docs can only be generated in a system with the
latter installed and are by default disabled.

.. _`PicoSDK Python wrappers`: https://pypi.org/project/picosdk/
.. _`PicoSDK library`: https://www.picotech.com/downloads

To build the API docs for this submodule locally edit the
:code:`docs/hvl_ccb.dev.picotech_pt104.rst` file to remove the :code:`.. code-block::`
directive preceding the following directives:

.. code-block::

    .. inheritance-diagram:: hvl_ccb.dev.picotech_pt104
       :parts: 1


    .. automodule:: hvl_ccb.dev.picotech_pt104
       :members:
       :undoc-members:
       :show-inheritance:
