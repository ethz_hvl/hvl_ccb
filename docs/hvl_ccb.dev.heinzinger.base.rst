hvl\_ccb.dev.heinzinger.base
============================



.. inheritance-diagram:: hvl_ccb.dev.heinzinger.base
   :parts: 1


.. automodule:: hvl_ccb.dev.heinzinger.base
   :members:
   :undoc-members:
   :show-inheritance:
